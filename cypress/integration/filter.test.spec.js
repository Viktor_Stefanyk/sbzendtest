/// <reference types="cypress"/>

describe('Test Case 8. Home page. Filters', function() {
    beforeEach(() => {
        cy.visit('/');
        cy.get('.ssls-products-filters-holder > .ssls-products-filters').as('productsFilter');
        cy.get('.ssls-product-cards-holder .ssls-product-card').as('productCard');
        cy.get('.user-box > .search > button').as('search');
    });

    it('Filter by "Basic & Fast"', () => {

        let filterName = 'Basic & fast';

        let expectedResult = 'Domain Validation';

        cy.get('.ssls-section-page.ssls-products').within(() => {
            cy.get('@productCard').then(($card) => {
                this.cardQtyBeforeFilter = $card.length;
            })

            cy.get('@productsFilter').contains(filterName).click();

            cy.get('@productCard').within(($card) => {
                assert.notEqual($card.length, this.cardQtyBeforeFilter, 'The filter is worked');

                cy.get('div[class^="ssls-product-card__body-content"]')
                    .then(($content) => {
                        for (let i = 0; i < $content.length; i++) {
                            cy.get($content[i]).find('p[class="text"]').then((text) => {
                                let pText = text.contents().text();
                                expect(pText).to.include(expectedResult);
                            });
                        }
                    });
            });

        });

        cy.get('@productsFilter').contains(filterName).click();

        cy.get('.ssls-section-page.ssls-products')
            .find('.ssls-product-cards-holder .ssls-product-card')
            .then(($newCard) => {
                assert.equal($newCard.length, this.cardQtyBeforeFilter);
            });
    });

    it('Filter by "Business" and "One site"', () => {

        let expectedResult1 = '1 domain';
        let expectedResult2 = 'Organization Validation';
        let business = 'Business';
        let oneSite = 'One site';

        cy.get('.ssls-section-page.ssls-products').within(() => {
            cy.get('@productCard').then(($card) => {
                this.cardQtyBeforeFilter = $card.length;
            })

            cy.get('@productsFilter').contains(business)
                .click()
                .get('@productsFilter').contains(oneSite)
                .click();

            cy.get('@productCard').within(($card) => {
                assert.notEqual($card.length, this.cardQtyBeforeFilter, 'The filter is worked');

                cy.get('div[class^="ssls-product-card__body-content"]')
                    .then(($content) => {
                        for (let i = 0; i < $content.length; i++) {
                            cy.get($content[i]).find('p[class="text"]').then((text) => {
                                let pText = text.contents().text();
                                expect(pText).to.include(expectedResult1).and.to.include(expectedResult2);
                            });
                        }
                    });
            });
        });

        cy.get('@productsFilter').contains(business)
            .click()
            .get('@productsFilter').contains(oneSite)
            .click();

        cy.get('.ssls-section-page.ssls-products')
            .find('.ssls-product-cards-holder .ssls-product-card')
            .then(($newCard) => {
                assert.equal($newCard.length, this.cardQtyBeforeFilter);
            });

    });

    it('Search', () => {
        let desiredValue = 'Essential';

        cy.get('.ssls-home-page')
            .find('.ssls-section-page.ssls-products')
            .as('productSection').within(($header) => {
                cy.get($header).find('div[class="ssls-heading"]').should('exist')
                cy.get($header).find('div[class="ssls-products-filters-holder"]').should('exist')
                cy.get($header).find('div[class="ssls-search"]').should('not.exist')
            })

        cy.get('@search').click();

        cy.url().should('include', '/search');

        cy.get('.ssls-home-page')
            .find('.ssls-section-page.ssls-products')
            .as('productSection').within(($header) => {
                cy.get($header).find('div[class="ssls-heading"]').should('exist')
                cy.get($header).find('div[class="ssls-products-filters-holder"]').should('not.exist')
                cy.get($header).find('div[class="ssls-search"]').should('exist')
            })

        cy.get('.ssls-search > .ssls-container > input').type(desiredValue);

        cy.get('@productCard')
            .find('a[class^="ssls-product-card__header"]')
            .find('h3')
            .then(($title) => {
                for (let i = 0; i < $title.length; i++) {
                    cy.get($title[i]).should('contain.text', desiredValue);
                }
            });


    })

})