/// <reference types="Cypress"/>

describe('My Profile page', function() {

    beforeEach(() => {

        cy.fixture('valid.credential.json').as('credential').then((credential) => {
            this.credential = credential;
        });
        cy.fixture('user/user.json').as('user').then((user) => {
            this.user = user;
        });

        cy.visit('/');

        cy.get('.log-box').children('a[class*="btn flat-dark"]').contains('Log in').click();
        cy.url().should('include', '/authorize');

        cy.get('.email-box').children('form[name="authForm"]').within(() => {
            cy.get('input[name="email"]').type(this.credential.email);
            cy.get('input[name="password"]').type(this.credential.password);
            cy.root().submit();
        });
    });

    it('Test Case 6 (My profile page. Client area)', () => {

        cy.get('.log-box').children('.profile-box').as('logBox');

        cy.get('@logBox')
            .find('a[class="btn btn-s round filled user-btn ng-binding"]')
            .should('contain.text', this.credential.email);

        //Check the dropdown menu
        cy.get('@logBox')
            .children('button[class="btn btn-s round filled dropdown-btn ng-isolate-scope"]')
            .click();

        cy.get('@logBox')
            .find('ul[class^="dropdown"] > li > a')
            .contains('View profile')
            .click();
        cy.url().should('include', '/user/profile');

        cy.get('.panel > form').within(($form) => {



            cy.get('.item').then((items) => {
                for (let i = 0; i < items.length; i++) {
                    cy.get(items[i]).within(() => {
                        cy.get('.terms > .text').then(($termName) => {
                            let termName = $termName.text().trim();
                            if (termName != 'Newsletter') {
                                cy.get('.description > .text').then(($fieldText) => {
                                    let fieldText = $fieldText.text().trim();
                                    cy.log($fieldText.text());
                                    expect(this.user[Object.keys(this.user)[i]]).to.eq(fieldText);
                                });
                            } else {
                                cy.get('.description').find('.toggle-btn').should('not.have.class', this.user.newsLetter)
                            }
                        });
                    });
                }
            });


        });


    });

    it('Test Case 7 (My profile page. Refresh support pin)', () => {

        cy.get('.log-box').children('.profile-box').as('logBox');

        cy.get('@logBox')
            .find('a[class="btn btn-s round filled user-btn ng-binding"]')
            .should('contain.text', this.credential.email);

        //Check the dropdown menu
        cy.get('@logBox')
            .children('button[class="btn btn-s round filled dropdown-btn ng-isolate-scope"]')
            .click();

        cy.get('@logBox')
            .find('ul[class^="dropdown"] > li > a')
            .contains('View profile')
            .click();
        cy.url().should('include', '/user/profile');


        cy.get('.panel > form').within(($form) => {

            let supportPin;
            cy.get('.item').then((items) => {
                for (let i = 0; i < items.length; i++) {
                    cy.get(items[i]).within(() => {
                        cy.get('.terms > .text').then(($termName) => {
                            let termName = $termName.text().trim();
                            if (termName === 'Support pin') {
                                cy.get('.description > .text').then(($fieldText) => {
                                    supportPin = $fieldText.text().trim();
                                });
                                cy.get('button[name="supportPin"]').click();
                                cy.wait(1000);
                                cy.get('.description > .text').then(($fieldText) => {
                                    let fieldText = $fieldText.text().trim();
                                    expect(supportPin).to.not.eq(fieldText);
                                    this.user.supportPin = fieldText;
                                    cy.writeFile('cypress/fixtures/user/user.json', this.user);
                                });
                            }
                        });
                    });
                }
            });
        });
    });

});