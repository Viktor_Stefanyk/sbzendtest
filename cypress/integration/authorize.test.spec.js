/// <reference types="Cypress"/>

describe('Authorization page', function() {

    beforeEach(() => {
        cy.fixture('valid.credential.json').as('user').then((user) => {
            this.user = user;
        });

        cy.fixture('invalid.credential.json').as('user').then((invalidUser) => {
            this.invalidUser = invalidUser;
        });

        cy.fixture('error.messages.json').as('user').then((errorMessage) => {
            this.errorMessage = errorMessage;
        });

        cy.visit('/');

        //Click the Login Button
        cy.get('.log-box').as('logBox').children('a[class*="btn flat-dark"]').contains('Log in').click();
        cy.url().should('include', '/authorize');


        cy.get('.email-box').children('form[name="authForm"]').as('authForm');
        cy.get('@authForm').find('input[name="email"]').as('emailField');
        cy.get('@authForm').find('input[name="password"]').as('passwordField');
        cy.get('@authForm').find('button[class="btn-input btn-input-block"]').as("eyeButton");
    });

    it('Test Case 1', () => {
        
        login(this.user.email, this.user.password, true);
        
        cy.wait(2000);
        
        //Check if the user is logged
        cy.get('.log-box').find('a[class="btn btn-s round filled user-btn ng-binding"]').then($loggedUser => {
            expect(this.user.email).to.equal($loggedUser.text());
        });

        //Check the dropdown menu
        cy.get('.log-box').children('.profile-box')
            .children('button')
            .should('have.class', 'btn btn-s round filled dropdown-btn ng-isolate-scope');
    });

    it('Test Case 2 (Locked Account)', () => {

        login(this.invalidUser.email, this.invalidUser.password, true);
        
        cy.get('li[class="notification information"]')
            .find('span[class="noty_text"]').then(($notification) => {
                expect(this.errorMessage.wrongEmailOrPassword).to.equal($notification.text());
            })
    });

    it('Test Case 2.1 (Email or password is incorrect)', () => {

        login(this.user.email, this.invalidUser.password, true);

        cy.get('li[class="notification information"]').find('span[class="noty_text"]').then(($notification) => {
            expect(this.errorMessage.wrongEmailOrPassword).to.equal($notification.text());
        })
    });


    it('Test Case 2.2 (Expected result from TT)', () => {

        login(this.invalidUser.email, this.invalidUser.password, true);

        cy.get('li[class="notification information"]').find('span[class="noty_text"]').then(($notification) => {
            expect(this.errorMessage.wrongEmailOrPassword).to.equal($notification.text());
        })
    });

    it('Test Case 3 (Invalid email)', () => {

        login(this.invalidUser.incorrectEmail, this.invalidUser.password, true);
        
        cy.get('.left-tooltip-box')
            .not('.ng-hide')
            .find('div[class="tooltip tooltip-error"]')
            .find('span[class="tooltip-text"]').as('tooltipElement');

        cy.get('@tooltipElement')
            .then(($tooltip) => {
                expect(this.errorMessage.incorrectEmail)
                .to.equal($tooltip[0].innerHTML.trim());
            })
    });

    it('Test Case 4 (Empty email and password fields)', () => {

        //Click the Submit button
        cy.get('@authForm').submit();

        cy.get('.left-tooltip-box')
            .not('.ng-hide')
            .find('div[class="tooltip tooltip-error"]')
            .find('span[class="tooltip-text"]').as('tooltipElement');

        cy.get('@tooltipElement')
            .first()
            .then((emptyEmailField) => {
                expect(this.errorMessage.emptyEmailField)
                .to.eq(emptyEmailField.text().trim())
            });

        cy.get('@tooltipElement')
            .last().then((emptyPasswordField) => {
                expect(this.errorMessage.emptyPasswordField)
                .to.eq(emptyPasswordField.text().trim())
            });
    });

    it('Test Case 5 (Log out)', () => {

        login(this.user.email, this.user.password, false);

        cy.wait(2000);

        //Check if the user is logged
        cy.get('.log-box').children('.profile-box').as('logBox');

        cy.get('@logBox')
            .find('a[class="btn btn-s round filled user-btn ng-binding"]')
            .should('contain.text', this.user.email);

        //Check the dropdown menu
        cy.get('@logBox')
            .children('button[class="btn btn-s round filled dropdown-btn ng-isolate-scope"]')
            .click();

        cy.get('@logBox')
            .find('ul[class^="dropdown"] > li > button')
            .contains('Log out')
            .click();

        cy.url().should('include', '/authorize');
    });



})

function login(email, password, eye) {

    //Fill the Email and Password
    cy.get('@emailField').type(email);
    cy.get('@passwordField').type(password);

    if(eye) {
        //Click on the eye
        cy.get('@eyeButton').click();

        //Check the password
        cy.get('@passwordField').should('have.value', password); 
    }
    //Click the Submit button
    cy.get('@authForm').submit();

}