#### Install Cypress via npm
```sh
$ npm install cypress --save-dev
```
#### Invoke the command from your project root like so:

```sh
$ npm run cypress:open
```
...and Cypress will open right up for you.

#### Click on the button 'Run all spects' in the Cypress